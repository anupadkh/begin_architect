import { TestBed } from '@angular/core/testing';

import { WagtailApiService } from './wagtail-api.service';

describe('WagtailApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WagtailApiService = TestBed.get(WagtailApiService);
    expect(service).toBeTruthy();
  });
});
