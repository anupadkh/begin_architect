import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardsRoutingModule } from './Dashboards-routing.module';
import { WagtailApiService } from '../../api/wagtail-api.service';

@NgModule({
  imports: [
    CommonModule,
    DashboardsRoutingModule,
    // WagtailApiService,
  ],
  declarations: [],
  providers: [WagtailApiService,]
})
export class DashboardsModule { }
